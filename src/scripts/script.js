document.getElementById("task1Button").addEventListener("click", function() {
    displayResult('task1Display');
}, false);

document.getElementById("task2Button").addEventListener("click", function() {
    displayResult('task2Display', 2);
}, false);

document.getElementById("task3Button").addEventListener("click", function() {
    displayResult('task3Display', undefined, true);
}, false);

document.getElementById("task5__button").addEventListener("click", function(e) {
    const text = document.getElementsByClassName('task5__text')[0].value;
    alert(`Your name is ${text}`);
}, false);

document.getElementById("task6__button").addEventListener("click", function(e) {
    const age = document.getElementsByClassName('task6__text')[0].value;
    const result = age > 50 ? 'Wow!' : age < 18 ? 'Ваш возраст слишком маленький' : 'отличный возраст';
    alert(result);
}, false);

document.getElementById("task7__button").addEventListener("click", function(e) {
   const result = prompt('Enter password');
   switch (result) {
       case "Правильный пароль":
           alert("Добро пожаловать");
           break;
        case null:
            alert("Вход отменён.");
            break;
       default: alert("Пароль неверен!");
           break;
   }
}, false);

document.getElementById("task10__button").addEventListener("click", function(e) {
    const login = document.getElementsByClassName('task10__text')[0].value;
    const result = login === "Вася" ? 'Привет!' : login === "Директор" ? "Здравствуйте!" : login === "" ? "Нет логина" : "";
    alert(result);
}, false);

document.getElementById("task11__button").addEventListener("click", function(e) {
    const age = document.getElementsByClassName('task11__text')[0].value;
    const result = age === "" ? "Возраст не введён" : (age >= 16 && age <= 100) ? 'Хороший возраст' : 'Плохой возраст';
    alert(result);
}, false);

document.getElementById("task12__button").addEventListener("click", function() {
    const rand = Math.floor(Math.random() * (11 - 1) + 1);
    const arr = [rand];
    for (let index = 0; index < 6; index++) {
        if(index !== 0) {
            arr[index] = arr[index - 1] + 5;
        }
    }
    let result = arr[0];
    for (let index2 = 1; index2 < arr.length; index2++) {
        result += arr[index2];        
    }
    document.getElementsByClassName('task12__label1')[0].innerText = arr.toString();
    document.getElementsByClassName('task12__label2')[0].innerText = result;
}, false);

document.getElementById("task13__button").addEventListener("click", function() {
    const rand = Math.floor(Math.random() * (1000 - 1) + 1);
    const arr = [];
    for (let index = 0; index < rand; index++) {
        arr[index] = index;        
    }
    document.getElementsByClassName('task13__label1')[0].innerText = `Length is ${arr.length}`;
    document.getElementsByClassName('task13__label2')[0].innerText = `Last element is ${arr[arr.length - 1]}`;
}, false);

document.getElementById("task14__button").addEventListener("click", function() {
    const rand = Math.floor(Math.random() * (10 - 1) + 1);
    const arr = [];
    for (let index = 0; index < rand; index++) {
        arr[index] = index;        
    }
    arr.length === 1 ? arr[arr.length - 1] = "Предпоследний элемент" : arr[arr.length - 2] = "Предпоследний элемент";
    document.getElementsByClassName('task14__label1')[0].innerText = `Length is ${arr.length}`;
    document.getElementsByClassName('task14__label3')[0].innerText = arr.toString();
}, false);
const arrTask15 = [];
document.getElementById("task15__button1").addEventListener("click", function() {
    arrTask15.splice(0,0,'Джаз', 'Блюз');
    document.getElementsByClassName('task15__label1')[0].innerText = arrTask15.toString();
}, false);



document.getElementById("task15__button2").addEventListener("click", function() {
    arrTask15.splice(arrTask15.length - 1, 0, 'Рок');
    document.getElementsByClassName('task15__label2')[0].innerText = arrTask15.toString();
}, false);

document.getElementById("task15__button3").addEventListener("click", function() {
    arrTask15.splice(1, 0, 'Классика');
    arrTask15.splice(-1, 1);
    document.getElementsByClassName('task15__label3')[0].innerText = arrTask15.toString();
}, false);

document.getElementById("task15__button4").addEventListener("click", function() {
    arrTask15.splice(0, 1);
    document.getElementsByClassName('task15__label4')[0].innerText = arrTask15.toString();
}, false);

document.getElementById("task15__button5").addEventListener("click", function() {
    arrTask15.splice(0, 0, 'Рэп', 'Рэгги');
    document.getElementsByClassName('task15__label5')[0].innerText = arrTask15.toString();
}, false);

document.getElementById("task16__button").addEventListener("click", function() {
    calc = 0;
    calcArr.splice(0, calcArr.length);
    document.getElementsByClassName('task16__label1')[0].innerText = "";
    document.getElementsByClassName('task16__label2')[0].innerText = "";
    getInput();
    document.getElementsByClassName('task16__label1')[0].innerText = `${calcArr}`;
    document.getElementsByClassName('task16__label2')[0].innerText = `${calc}`;
}, false);

const greeting1 = 'Hello';
document.getElementById("task17__button").addEventListener("click", function() {    
    greet();
}, false);

document.getElementById("task18__button").addEventListener("click", function() {    
    getMin();
}, false);

document.getElementById("task19__button").addEventListener("click", function() {    
    pow();
}, false);

document.getElementById("task20__button").addEventListener("click", function() {    
    alert(isEven());
}, false);

document.getElementById("task21__button").addEventListener("click", function() {    
    alert(trim());
}, false);

document.getElementById("task22__button").addEventListener("click", function() {    
    alert(convertFloor());
}, false);

document.getElementById("task23__button").addEventListener("click", function() {
    const arr = ["fngp", "kgei", "fpos", "clfw"];
    document.getElementsByClassName('task23__label1')[0].innerText = arr;
    document.getElementsByClassName('task23__label2')[0].innerText = reverseArr(arr);
    document.getElementsByClassName('task23__label3')[0].innerText = reverseWord(arr);
}, false);

const reverseArr = function(elem) {
return elem.reverse();
}

const reverseWord = function(elem) {
    return elem.map(item => {
        return item.split("").reverse().join("");
    });
}

const convertFloor = function() {
    let input = prompt('Enter a floor...');
    let result = '';
    switch (true) {
        case isNaN(input):
            return 'It is not a number';
        case input < 0:
        case input > 13:
            return input;
        case input >= 0 && input <= 12:
            return ++input;
        default: 
            return 'This number is not in use.';
    }
}

const trim = function() {
    const input = prompt('Enter a sentence...');
    let result = '';
    if(input === null) {
        result = 'Nothing to trim...';
    } else {
        result = input.slice(1, -1);        
    }
    return result;
}

const isEven = function() {
    const number = prompt('Enter a number...');
    if(isNaN(number)) {
        alert('It is not a number');
        isEven();
    }
    return number === null ? 'Quit' : +number === 0 ? `${number} is neither even nor uneven` : +number % 2 === 0 ? `${number} is even` : `${number} is uneven`;

}

const pow = function() {
    let number1 = prompt('Enter first digit...');
    if(+number1 <= 1 || isNaN(+number1)) {
        pow();
    }
    let number2 = prompt('Enter second digit...');
    if(+number2 <= 1 || isNaN(+number2)) {
        pow();
    }
    const number3 = number1;
    do{
        number1 *= number3;
        --number2;
    } while(number2 > 1);
    alert(number1);
}

const getMin = function() {
    const num1 = prompt('Enter number #1');
    const num2 = prompt('Enter number #2');
    const result = num1 < num2 ? num1 : num1 > num2 ? num2 : 'Equal';
    alert(result);
}

const greet = function() {
    const name = prompt('Enter your name..');
    const greeting2 = 'Good day';
    const result = name !== 'Саша' ? greeting1 : greeting2;
    alert(`${result} ${ name}`);
}

let calc = 0;
const calcArr = [];
const getInput = function() {
    const num = prompt('Enter a number (15 to quit)');  
        if(num === null || isNaN(num)) {
            getInput();
        } else if(num !== '15') {
            calcArr.push(num);
            calc += +num;
            getInput();
        }
        return;
}
document.getElementById("task4Button").addEventListener("click", function() {
    const result = document.getElementById("task4Text").value;
    if(!result) {
        alert("You enterd nothing...");
    } else if (isNaN(result)) {
        alert("It is not a digit...");
    } else if (result <= 20) {
        alert("The number should be more than 20..");
    } else if (result % 2 !== 0) {
        alert("The number should be even ...");
    } else {
        alert("Thank you for paticipation!");
    }    
}, false);

const arrGenerator = function(numFrom, numTo, itterator) {    
    const arr = [];
    for (let i = numFrom; i < numTo; i += itterator) {        
        arr.push(i);
    }    
    return arr;
};

const getEvenNumbers = function(itterator, breaker) {
    const result = arrGenerator(17, 30, itterator).filter((item, index, arr) => {
        if(!breaker || (breaker && index + 2 < arr.length)) {
            return item % 2 === 0;
        }
    });    
    return result;
};

const displayResult = function(id, itterator = 1, breaker = false) {   
    const result = getEvenNumbers(itterator, breaker);   
    document.getElementById(id).innerText = result.length === 0 ? "None number is even" : result;
};

